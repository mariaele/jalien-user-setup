#! /bin/bash

function die() { echo "$USAGE"; echo "$1"; exit 1; }
USAGE="Usage: --[help|install|setup|magic]"
OPTS=$(getopt -n "$0"  -o "hism" --long "help,install,setup,magic"  -- "$@")
[ $? -ne 0 ] && die

function install() {
        wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
        sudo dpkg -i cvmfs-release-latest_all.deb
        rm -f cvmfs-release-latest_all.deb
        sudo apt update
        sudo apt install cvmfs
        sudo apt install environment-modules
}

function setup () {
        sudo cvmfs_config setup
        # check if /etc/auto.master.d/cvmfs.autofs contains /cvmfs /etc/auto.cvmfs
        # Create default local
        # https://alien.web.cern.ch/content/documentation/howto/site/setupcvmfs
        # Create the following file with the following content
        cat > /etc/cvmfs/default.local << EoF
CVMFS_REPOSITORIES=alice.cern.ch
CVMFS_CLIENT_PROFILE=single
CVMFS_HTTP_PROXY=DIRECT
CVMFS_QUOTA_LIMIT=20000
EoF

}

function magic () {
        sudo cvmfs_config setup
        sudo service autofs start
        cvmfs_config probe
        sudo cvmfs_config chksetup
}


eval set -- "$OPTS"

while true;
do
    case "$1" in
        -h|--help)
            echo "$USAGE"
            exit
            ;;

        -i|--install)
                install
                shift
            ;;

        -s|--setup)
                setup
            shift
            ;;
        -m|--magic)
                magic
            shift
            ;;
        --)
            shift
            break;;
    esac
done